# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    count.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/09 16:08:09 by josaykos          #+#    #+#              #
#    Updated: 2020/03/10 13:02:30 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import string
import sys

def text_analyzer(
        *argv):
    """
    This function displays the sums of upper-case, lower-case, punctuation
    and spaces characters in a given text.
    text: text to analyze.
    """
    if len(argv) == 1:
        text = argv[0]
        countUpper = 0
        countLower = 0
        countSpace = 0
        countPunctuation = 0
        if (text == ""):
            print('What is the text to analyse ?')
            text_analyzer(input())
        else:
            for char in text:
                if char.isupper():
                    countUpper += 1
                elif char.islower():
                    countLower += 1
                elif char in string.punctuation:
                    countPunctuation += 1
                elif char.isspace():
                    countSpace += 1
            totalCount = countUpper + countLower + countSpace + countPunctuation
            print(f'The text contains ${totalCount} characters:\n')
            print(f'- ${countUpper} upper letters\n')
            print(f'- ${countLower} lower letters\n')
            print(f'- ${countPunctuation} punctuation marks\n')
            print(f'- ${countSpace} spaces\n')
            print('- %s spaces\n'(countSpace))
    else:
        print('ERROR')

