# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    operations.py                                      :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/09 17:09:00 by josaykos          #+#    #+#              #
#    Updated: 2020/03/09 19:05:23 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys

def main():
    if (len(sys.argv) == 3):
        if sys.argv[1].isdigit() and sys.argv[2].isdigit():
            calculator(sys.argv[1], sys.argv[2])
        else:
            print('InputError: only numbers\n')
            print('Usage: python operations.py <number1> <number2>')
            print('Example:')
            print('{:>3s} python operations.py 10 3'.format(' '))

    elif (len(sys.argv) < 3):
        print('Usage: python operations.py <number1> <number2>')
        print('Example:')
        print('{:>3s} python operations.py 10 3'.format(' '))
    elif (len(sys.argv) > 3):
        print('InputError: too many arguments\n')
        print('Usage: python operations.py <number1> <number2>')
        print('Example:')
        print('{:>3s} python operations.py 10 3'.format(' '))
    print()


def modulo(number1, number2):
    if number2 != 0:
        print('{:<13} {:.17g}'.format('Quotient: ', number1 % number2))
    else:
        print('{:<13} ERROR (modulo by zero)'.format('Quotient: '))


def divide(number1, number2):
    if number2 != 0:
        print('{:<13} {:.17g}'.format('Quotient: ', number1 / number2))
    else:
        print('{:<13} ERROR (div by zero)'.format('Quotient: '))


def calculator(arg1, arg2):
    number1 = float(arg1)
    number2 = float(arg2)
    print('{:<13} {:.0f}'.format('Sum: ', number1 + number2))
    print('{:<13} {:.0f}'.format('Difference: ', number1 - number2))
    print('{:<13} {:.0f}'.format('Product: ', number1 * number2))
    divide(number1, number2)
    modulo(number1, number2)


main()
