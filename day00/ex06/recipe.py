# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    recipe.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/09 20:20:09 by josaykos          #+#    #+#              #
#    Updated: 2020/03/10 13:52:10 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys

cookbook = {    'sandwich': {   'ingredients': ['ham', 'bread', 'cheese',
                                                'tomatoes'],
                                'meal': 'lunch',
                                'prep_time': 10,  },
                'cake': {   'ingredients':['flour', 'sugar', 'eggs'],
                            'meal': 'dessert',
                            'prep_time': 60,  },
                'salad': {  'ingredients':['avocado', 'arugula', 'tomatoes',
                            'spinach'],
                            'meal': 'lunch',
                            'prep_time': 15,   },   }


def main():
    option = ' '
    while (1):
        if not option.isdigit():
             print('Please select an option by typing the corresponding number:')
             print('1: Add a recipe')
             print('2: Delete a recipe')
             print('3: Print a recipe')
             print('4: Print the cookbook')
             print('5: Quit')
             option = input('>> ')
             print()
        if (option == '1'):
            print("Please enter the new recipe's name:")
            recipeName = input('>> ')
            print("Please enter the new recipe's ingredients:")
            ingredients = input('>> ')
            print("Please enter the new recipe's meal type:")
            meal = input('>> ')
            print("Please enter the new recipe's preparation time:")
            prepTime = input('>> ')
            print()
            add_recipe(recipeName, ingredients, meal, prepTime)

        elif (option == '2'):
            print('Please enter the recipe you want to delete:')
            recipeName = input('>> ')
            print()
            del_recipe(recipeName)
        elif (option == '3'):
            print("Please enter the recipe's name to get its details:")
            option = input('>> ')
            print()
            print_recipe(option)
        elif (option == '4'):
            print_cookbook()
        elif (option == '5'):
            sys.exit("Cookbook closed.")
        else:
            print("This option does not exist, please type the corresponding number.")
            print("To exit, enter 5.")
            option = input('>> ')
            print()


def print_cookbook():
    print('Recipes available in cookbook:')
    for key in cookbook:
        print(f'- {key}')
    print()



def print_recipe(recipeName):
        for key, value in cookbook.items():
            if recipeName == key:
                print (f'Recipe for {key}:')
                for instruction, detail in value.items():
                    if instruction == 'ingredients':
                        print(f'Ingredients list: {detail}')
                    if instruction == 'meal':
                        print(f'To be eaten for {detail}.')
                    if instruction == 'prep_time':
                        print(f'Takes {detail} mn of cooking.')
        print()


def add_recipe(newRecipe, ingredients, meal, prepTime):
    newEntry = {newRecipe : {   'ingredients': list(ingredients.split(' ')),
                                'meal': meal,
                                'prep_time': prepTime,      }}
    cookbook.update(newEntry)


def del_recipe(recipeName):
    del cookbook[recipeName]


main()
