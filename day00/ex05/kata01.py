# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    kata01.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/09 19:27:40 by josaykos          #+#    #+#              #
#    Updated: 2020/03/09 19:35:58 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

def main():
    print_dictionary()


def print_dictionary():
    languages = {
            'Python': 'Guido van Rossum',
            'Ruby': 'Yukihiro Matsumoto',
            'PHP': 'Rasmus Lerdorf',
            }
    for key, value in languages.items():
        print(f'{key} was created by {value}')


main()
