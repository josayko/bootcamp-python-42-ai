# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    kata03.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/09 19:47:14 by josaykos          #+#    #+#              #
#    Updated: 2020/03/09 20:04:51 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

def main():
    right_aligned()


def right_aligned():
    phrase = "The right format"
    print('{:{fill}>42}'.format(phrase, fill='-'), end='')


main()
