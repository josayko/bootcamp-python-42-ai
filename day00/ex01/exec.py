# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    exec.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/09 10:33:32 by josaykos          #+#    #+#              #
#    Updated: 2020/03/09 15:42:32 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys

def main():
    rev_alpha()

def rev_alpha():
    argc = 0
    for str in sys.argv[:0:-1]:
        newStr = list(str)
        for i, char in enumerate(newStr):
            if char.islower() == True:
                newStr[i] = char.upper()
            else:
                newStr[i] = char.lower()
        print(''.join(newStr[::-1]), end='')
        argc += 1
        if argc < len(sys.argv) - 1:
            print(' ', end='')
        else:
            print()

main()
