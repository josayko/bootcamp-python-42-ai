# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    filterwords.py                                     :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/10 12:10:20 by josaykos          #+#    #+#              #
#    Updated: 2020/03/10 15:13:25 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys
import string

def main():
    filter_words(sys.argv[1], sys.argv[2])


def filter_words(text, length):
    originalList = text.split()
    finalList = []
    for word in originalList:
        newWord = ""
        for char in word:
            if char not in string.punctuation:
                newWord += char
        if newWord != "":
            finalList.append(newWord)
    for item in finalList:
        if len(item) <= int(length):
            finalList.remove(item)
    print(finalList)
    print(finalList[0])


main()
