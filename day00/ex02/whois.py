# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    whois.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/09 15:39:23 by josaykos          #+#    #+#              #
#    Updated: 2020/03/09 16:06:54 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys

def main():
    odd_or_even()


def odd_or_even():
    if len(sys.argv) != 2 or not sys.argv[1].isdigit():
        print('ERROR')
    else:
        nb = int(sys.argv[1])
        if nb == 0:
            print("I'm Zero.")
        elif nb % 2:
            print("I'm Odd.")
        else:
            print("I'm Even.")

main()
