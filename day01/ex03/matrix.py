# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    matrix.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/12 12:41:44 by josaykos          #+#    #+#              #
#    Updated: 2020/03/17 17:48:56 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from vector import Vector

class Matrix:
    def __init__(self, *data):
        self.data = [[]]
        self.shape = []
        if len(data) < 1:
            self.shape = ()
        elif len(data) == 1:
            if isinstance(data[0], list):
                self.data = list(data[0])
                item_nb = 0
                for element in data[0]:
                    if item_nb == 0:
                       item_nb = len(element)
                    elif item_nb != len(element):
                       print('Error: incorrect matrix dimensions.')
                       return
                    else:
                       self.shape = (len(data[0]), item_nb)
            elif isinstance(data[0], tuple):
                if len(data[0]) == 2:
                    self.shape = data[0]
                    newElement = list()
                    for _x in range(data[0][0]):
                        newElement.append(0)
                    for _y in range(data[0][1]):
                        self.data[0].append(newElement)
                    self.data = list(self.data[0])
                else:
                    print('Error: incorrect matrix dimensions (rows, columns))')
                    return
        elif len(data) == 2:
            if isinstance(data[0], list) and isinstance(data[1], tuple):
                elementLen = 0
                for element in data[0]:
                    if elementLen == 0:
                        elementLen = len(element)
                    elif elementLen != len(element):
                        print('Error: incorrect matrix dimensions (incorrect data size)')
                        return
                if len(data[1]) == 2:
                    if len(data[0]) != data[1][0]:
                        print('Error: incorrect matrix dimensions (rows, columns)')
                        return
                    if elementLen != data[1][1]:
                        print('Error: incorrect matrix dimensions (rows, columns)')
                        return
                    self.data = list()
                    self.data.append(data[0])
                    self.data = list(self.data[0])
                    self.shape = data[1]
                else:
                    print('Error: incorrect matrix dimensions (rows, columns)')
                    return
        else:
            print('Error: incorrect arguments in Matrix class.')

    def __add__(self, other):
        result = []
        if isinstance(other, Matrix):
            if self.shape == other.shape:
                for x, y in zip(self.data, other.data):
                    newElement = [value1 + value2 for value1, value2 in zip(x, y)]
                    result.append(newElement)
                return Matrix(result)

    def __sub__(self, other):
        result = []
        if isinstance(other, Matrix):
            if self.shape == other.shape:
                for x, y in zip(self.data, other.data):
                    newElement = [value1 - value2 for value1, value2 in zip(x, y)]
                    result.append(newElement)
                return Matrix(result)

    def __mul__(self, other):
        result = []
        if isinstance(other, Matrix):
            if self.shape[1] == other.shape[0]:
                for x in other.value:
                    print(x)
        elif isinstance(other, (int, float)):
            for element in self.data:
                newElement = [other * x for x in element]
                result.append(newElement)
            return Matrix(result)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __repr__(self) :
        return '(' + self.__class__.__name__ + ' ' + str(self.data) + ')'
