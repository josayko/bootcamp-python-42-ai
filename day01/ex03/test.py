# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/12 12:41:54 by josaykos          #+#    #+#              #
#    Updated: 2020/03/17 17:46:52 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from matrix import Matrix

m0 = Matrix()
print(m0)
print(m0.shape)
m1 = Matrix([[1, 2], [3, 4]])
print(m1)
print(m1.shape)
m2 = Matrix([[1, 2, 4], [5, 6, 7], [8, 9, 10]])
print(m2)
print(m2.data)
print(m2.shape)
m3 = Matrix((3, 3))
print(m3)
print(m3.shape)
m4 = Matrix([[1, 2, -2], [3, 4, 0], [5, 6, 3]], (3, 3))
print(m4)
print(m4.data)
print(m4.shape)
m5 = 2 * m2
print(m5)
print(m5.data)
print(m5.shape)
