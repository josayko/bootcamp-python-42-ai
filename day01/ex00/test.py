# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/10 15:31:37 by josaykos          #+#    #+#              #
#    Updated: 2020/03/11 16:00:28 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from book import Book
from recipe import Recipe

def main():
	# create recipes
	sushi = Recipe('sushi', 2, 90, ['rice', 'nori', 'fish'], 'Authentic japanese recipe', 'lunch')
	cake = Recipe('cake', 1, 45, ['eggs', 'flour', 'milk'], 'Classic cake recipe', 'dessert')
	# create book and add the recipes
	book1 = Book("My recipes's book")
	book1.add_recipe(sushi)
	book1.add_recipe(cake)

	print(book1.recipes_list)
	print()
	book1.get_recipe_by_name('sushi')
	print()
	book1.get_recipe_by_name('pot-au-feu')
	print()
	book1.get_recipe_by_name('cake')
	print()
	pizza = ['flour', 'tomato', 'cheese']
	book1.add_recipe(pizza)
	print()
	to_print = str(cake)
	print(to_print)
	print()
	book1.get_recipes_by_types('dessert')
	print()
	book1.get_recipes_by_types('lunch')
	print()
	book1.get_recipes_by_types('brunch')
	print()

	pasta = Recipe()
	book1.add_recipe(pasta)
	print(str(pasta))
	print(book1.creation_date)
	print(book1.last_update)

main()
