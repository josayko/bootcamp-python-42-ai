# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    book.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/10 15:31:30 by josaykos          #+#    #+#              #
#    Updated: 2020/03/11 16:00:10 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from datetime import datetime
from recipe import Recipe

class Book:
	recipes_list = { 	'starter': [],
						'lunch': [],
						'dessert': [], 	}

	def __init__(self, name='a book'):
		self.name = name
		self.creation_date = datetime.now()
		self.last_update = datetime.now()

	def get_recipe_by_name(self, name):
		for key in self.recipes_list:
			for value in self.recipes_list[key]:
				if value == name:
					print(f'{name.capitalize()} recipe:')
					for description, detail in self.recipes_list[key][value].items():
						print(f'{description}: {detail}')

		print(f"{name.capitalize()}'s recipe not found in '{self.name}' !")

	def get_recipes_by_types(self, recipe_type):
		for key in self.recipes_list:
			if recipe_type == key:
				print(f'{key.capitalize()} list:')
				for value in self.recipes_list[key]:
					print(f'{value}')
				return
		print(f'ERROR: {recipe_type} not found.')

	def add_recipe(self, recipe):
		if isinstance(recipe, Recipe):
			recipeDetails = {recipe.name: {	'cooking lvl': recipe.cooking_lvl,	 	 							'cooking time': recipe.cooking_time,
											'ingredients': recipe.ingredients,
											'description': recipe.description,
											'recipe_type': recipe.recipe_type, }}
			for key in self.recipes_list.keys():
				if recipe.recipe_type == key:
					self.recipes_list[key] = recipeDetails
					self.last_update = datetime.now()
			return (self)
		else:
			print(f'ERROR: {recipe} is not a recipe (<class Recipe> required)')

