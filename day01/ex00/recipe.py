# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    recipe.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/10 15:31:34 by josaykos          #+#    #+#              #
#    Updated: 2020/03/11 14:27:43 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Recipe:
	def __init__(self, name='a recipe', cooking_lvl=0, cooking_time=0, ingredients='nothing', description="", recipe_type = 'unknown'):
		self.name = name
		self.cooking_lvl = cooking_lvl
		self.cooking_time = cooking_time
		self.ingredients = ingredients
		self.description = description
		self.recipe_type = recipe_type

	def __str__(self):
		txt = f"The recipe {self.name} has been created."
		for key, value in self.__dict__.items():
			print(f'{key}: {value}')
		return(txt)
