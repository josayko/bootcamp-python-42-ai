# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    game.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/11 15:02:35 by josaykos          #+#    #+#              #
#    Updated: 2020/03/11 16:38:17 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class GotCharacter:
	def __init__(self, first_name, is_alive=True):
		self.first_name = first_name
		self.is_alive = is_alive


class Stark(GotCharacter):
	"""A class representing the Stark family. Or when bad things happen to good people."""
	house_words = "Winter is coming"
	def __init_(self, house_words, family_name="Stark", first_name=None, is_alive=True):
		super().__init__(first_name=first_name, is_alive=is_alive)
		self.family_name = family_name
		self.house_words = house_words

	def print_house_words(self):
		print(self.house_words)

	def die(self):
		self.is_alive = False
