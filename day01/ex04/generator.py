# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    generator.py                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/19 11:25:03 by jonny             #+#    #+#              #
#    Updated: 2020/03/19 12:43:19 by jonny            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from random import shuffle

def generator(text, sep=" ", option=None):
    if isinstance(text, str):
        word_list = text.split(sep)
        if option == None:
            pass
        elif option == "shuffle":
            shuffle(word_list)
        elif option == "ordered":
            word_list = sorted(word_list)
        elif option == "unique":
            word_list = list(set(word_list))
        else:
            print(f'Error: invalid option <{option}>')
            return
        for word in word_list:
            yield (word)
    else:
        print('Error: first argument is not str.')

