# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    eval.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/22 10:03:33 by jonny             #+#    #+#              #
#    Updated: 2020/03/22 11:03:01 by jonny            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Evaluator:
    @staticmethod
    def zip_evaluate(words, coefs):
        if len(words) != len(coefs):
            return -1
        words_len = list()
        for word in words:
            words_len.append(len(word))
        new_list = (x * y for x, y in zip(words_len, coefs))
        return sum(new_list)

    @staticmethod
    def enumerate_evaluate(words, coefs):
        if len(words) != len(coefs):
            return -1
        new_list = list()
        for index, value in enumerate(words):
            new_list.append(len(value) * coefs[index])
        return(sum(new_list))

