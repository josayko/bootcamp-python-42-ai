# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    vector.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/11 16:40:24 by josaykos          #+#    #+#              #
#    Updated: 2020/03/12 12:31:15 by josaykos         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Vector:
	def __init__(self, *values):
		if not values:
			self.values = [0]
		else:
			self.values = values
		self.size = len(values)

	def __add__(self, other):
		if type(other) == type(self) and len(self.values[0]) == len(other.values[0]):
			return Vector([x + y for x, y in zip(self.values[0], other.values[0])])
		else:
			print('Error: different size vectors')
			return ('undefined')

	def __radd__(self, other):
		print('Error: cannot perform addition  between vector and scalar')
		return ('undefined')

	def __sub__(self, other):
		if type(other) == type(self) and len(self.values[0]) == len(other.values[0]):
			return Vector([x - y for x, y in zip(self.values[0], other.values[0])])
		else:
			print('Error: different size vectors')
			return ('undefined')

	def __rsub__(self, other):
		print('Error: cannot perform substraction between vector and scalar')
		return ('undefined')

	def __mul__(self, other):
		if type(other) == type(self):
			if len(self.values[0]) == len(other.values[0]):
				return Vector([x * y for x, y in zip(self.values[0], other.values[0])])
			else:
				return ('Error: different size vectors')
		else:
			return Vector([other * x for x in self.values[0]])

	def __rmul__(self, other):
			return self.__mul__(other)

	def __repr__(self):
		return '(' + self.__class__.__name__ + ' ' + str(self.values[0]) + ')'
