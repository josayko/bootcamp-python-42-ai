# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    ft_map.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/22 11:15:52 by jonny             #+#    #+#              #
#    Updated: 2020/03/22 11:48:42 by jonny            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

def generator(new_list):
    for element in new_list:
        yield element

def ft_map(function_to_apply, list_of_inputs):
    new_list = []
    for element in list_of_inputs:
        new_list.append(function_to_apply(element))
    return (generator(new_list))

